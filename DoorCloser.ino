boolean onUno = false;

const int MIC = A2;
const int EN = onUno ? 3 : 0;
const int IN1 = onUno ? 4 : 1;
const int IN2 = onUno ? 5 : 2;

const int clapDelay = 350; //clap delay to aim for
const int clapDelaySens = 100; //window for the second clap
const int clapThreshold = 400; //mic volume to detect a "clap"
unsigned long lastClap = 0;

unsigned long closeTime = 7000;
unsigned long releaseTime = 4750;

void setup() {
  //Serial.begin(9600);

  pinMode(EN, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
}

void loop() {
  int micReading = analogRead(MIC); 
  
  //debug(String(micReading));

  if(analogRead(MIC) >= clapThreshold) {
    //debug("Clap detected");
    unsigned long currentTime = millis();
    unsigned long interval = currentTime - lastClap;
    
    if(abs(interval - clapDelay) <= clapDelaySens)
      closeDoor();
    
    lastClap = currentTime;
  }
}

void closeDoor() {
  debug("Closing door");
  
  digitalWrite(EN, HIGH);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);

  delay(closeTime);

  digitalWrite(EN, LOW);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);

  delay(300);

  debug("Releasing string");
  digitalWrite(EN, HIGH);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);

  delay(releaseTime);

  debug("Shutting off motor");
  digitalWrite(EN, LOW);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}

void debug(String msg) {
  //Serial.println(msg);
}

